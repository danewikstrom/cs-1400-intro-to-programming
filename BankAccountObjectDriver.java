import java.util.*;

public class BankAccountObjectDriver
{

   public static void main (String[] args)
   {
   
      Scanner in = new Scanner (System.in);
      BankAccount bankA;
   
      String name = "";
      int deposit = 0;
      int withdraw = 0;
      int userInput = 0;
      
      System.out.println ("Welcome. Please enter your name to create an account.");
      name = in.nextLine();
      
      bankA = new BankAccount (name, 54321);
      
      do
      {   
         System.out.println (" ");  
         System.out.println ("To make a deposit, enter 1.");
         System.out.println ("To make a withdraw, enter 2.");
         System.out.println ("To view your account information, enter 3.");
         System.out.println ("To quit, enter 4.");
         userInput = in.nextInt();
      
         while (userInput != (1) && userInput != (2) && userInput != (3) && userInput != (4))//checking for valid entry
         {
            System.out.println ("Invalid input.");
            userInput = in.nextInt();
         }
         
         if(userInput == 1)//deposit
         {
            System.out.println ("Enter the amount you would like to deposit.");
            deposit = in.nextInt();
            bankA.deposit (deposit);
         }
         
         if(userInput == 2)//withdraw
         {
            System.out.println ("Enter the amount you would like to withdraw.");
            withdraw = in.nextInt();
            bankA.withdraw (withdraw);
         }
         
         if(userInput == 3)//print account info
         {
            System.out.println (" ");
            System.out.println ("Name: " + bankA.getName());
            System.out.println ("Account number: " + bankA.getAccountNum());
            System.out.println ("Balance: " + bankA.getBalance());
            System.out.println (" ");
         }
      
      }while (userInput == (1) || userInput == (2) || userInput == (3));     
   
   }

}