import java.util.Scanner;
import java.util.Random;

public class rockPaperScissors1
{

   public static void main (String[] args)  
   {
   
      Scanner in = new Scanner (System.in);
      Random rps = new Random ();
      
      int rounds = 0;
      int cpuRandom = 0;//temp var to decide cpu guess
      String cpu = "";//computer guess
      String userInput = "";//user guess
      int cpuWins = 0;//to count number of computer wins
      int userWins = 0;//to count number of user wins
      
      System.out.println ("Welcome to Rock, Paper, Scissors! \nEnter an odd number of rounds that you would like to play.");
      rounds = in.nextInt();
      
      in.nextLine();//flush the buffer
      
      while (rounds % 2 == 0)//determining if user's choice of number of rounds is odd or even
      {
         System.out.println ("In order to have a winner, the number of rounds needs to be an odd number. \nPlease enter an odd number of rounds you'd like to play.");
         rounds = in.nextInt();
      }
     
      
      for (int i = 0; i < rounds; i++)
      {
      
         System.out.println ("Type R for rock, P for paper, or S for scissors.");
         userInput = in.nextLine();
        
         while (!userInput.equalsIgnoreCase ("R") && (!userInput.equalsIgnoreCase ("P") && (!userInput.equalsIgnoreCase ("S"))))//check for valid input
         {
            System.out.println ("Invalid entry. Type R for rock, P for paper, or S for scissors.");
            userInput = in.nextLine();
         }
         
         cpuRandom = rps.nextInt(3);
         
         if (cpuRandom == 0)
         {
            cpu = "R";
         }
         
         if (cpuRandom == 1)
         {
            cpu = "P";
         }
         
         if (cpuRandom == 2)
         {
            cpu = "S";
         }
         
         System.out.println ("The computer guessed " + cpu + ".");
         
         if (userInput.equalsIgnoreCase (cpu))
         {
            System.out.println ("It's a tie! Play again.");
            System.out.println (" ");
            rounds++;
         }
         
         else //decide who wins the rounds
         {
            if (userInput.equalsIgnoreCase ("R") && cpu.equalsIgnoreCase ("S") || (userInput.equalsIgnoreCase ("P") && cpu.equalsIgnoreCase("R") || (userInput.equalsIgnoreCase ("S") && cpu.equalsIgnoreCase ("P"))))
            {
               System.out.println ("You won!");
               System.out.println (" ");
               userWins++;
            }
            
            if (cpu.equalsIgnoreCase ("R") && userInput.equalsIgnoreCase ("S") || (cpu.equalsIgnoreCase ("P") && userInput.equalsIgnoreCase ("R") || (cpu.equalsIgnoreCase ("S") && userInput.equalsIgnoreCase ("P"))))          
            {
               System.out.println ("You lost!");
               System.out.println (" ");
               cpuWins++;
            }
         
         }//end else
      
      }//end for
      
      System.out.println ("You won " + userWins + " rounds and the computer won " + cpuWins + " rounds.");
      
      if (userWins > cpuWins)
      {
         System.out.println ("You won the game!");
      }
      
      if (userWins < cpuWins)
      {
         System.out.println ("You lost the game!");
      }
   
   }

}