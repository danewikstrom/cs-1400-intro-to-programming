import java.util.Scanner;
import java.util.Random;

public class randomNumberGame
{

   public static void main (String[] args)
   {
    
      Scanner in = new Scanner (System.in);
      Random numberGenerator = new Random();
      
      int userGuess = 0;
      int winningNumber = 0;
      int numberOfGuesses = 0;
      
      //random number generator for winning number
      winningNumber = numberGenerator.nextInt(100) + 1;
   
   
      System.out.println ("I'm thinking of a number between 1 and 100. Try and guess what number I'm thinking of.");
      userGuess = in.nextInt();
      
     //checking for valid numbers 
      while (userGuess > 100 || userGuess < 1)
      {
         System.out.println ("The number you guessed is not inbetween 1 and 100. \nTry again.");
         userGuess = in.nextInt();
      }
   
     //telling user if input is greater or less than winning number or correct
      while (userGuess > winningNumber)
      {
         System.out.println ("The number I'm thinking of is less than " + userGuess + ". \nTry again.");
         userGuess = in.nextInt();
         numberOfGuesses++;
      }
        
      while (userGuess < winningNumber)
      {
         System.out.println ("The number I'm thinking of is greater than " + userGuess + ". \nTry again.");
         userGuess = in.nextInt();
         numberOfGuesses++;
      }
     
      if (userGuess == winningNumber);
      {
         System.out.println ("You've won!.");
         System.out.println ("");
         numberOfGuesses++;
      }
     
     //Print number of guesses it took the user to guess the winning number
      System.out.println ("It took you " + numberOfGuesses + " times to guess the winning number.");
    
   
   }


}