import java.util.*;

public class Roulette
{

   public static double betOnce (Scanner console, double bet)
   {
   
      Random r = new Random();
      
      int number = r.nextInt(37);  
      int userChoice = 0;
      int userGuess = 0;
      
      System.out.println ("Do you want to bet on 1) low or 2) high? or 3) a Number?");
      userChoice = console.nextInt();
      
      while(userChoice != (1) && userChoice != (2) && userChoice != (3))
      {
         System.out.println ("Invalid input. Only 1 (representing low) or 2 (representing high) are valid inputs. \nDo you want to bet on 1) low or 2) high or 3) a Number?");
         userChoice = console.nextInt();
      }
      
         if (number == 0) //if number = 0 
         {
            System.out.println ("The number was " + number + ".");          
            System.out.println("You've lost your bet.");
            bet = bet - bet;
         }
         
         if (userChoice == 3)//if user selects 3
         {
         System.out.println ("Enter a guess between 1 and 36.");
         userGuess = console.nextInt();
   
         }
         
         if (userChoice == 3 && userGuess == number)// if user guesses correct
         {
            System.out.println ("You've won!");
            bet = bet*34;
         }
            
         if (userChoice == 3 && userGuess != number)// if user guesses incorrect
         {
            System.out.println ("You've losed your bet!");
            System.out.println ("The number was " + number + ".");
            bet = bet - bet;
         }
             
      
      else // if wins or losses
      {      
         
         if ((userChoice == 1 && number <=18) || (userChoice == 2 && number >18 ))//win
         {
            System.out.println ("The number was " + number + ".");
            System.out.println ("You've won!");          
            bet = bet*2;//doubles bet
         }
         
         else if ((userChoice == 1 && number >18) || (userChoice == 2 && number <=18))//lose
         {
            System.out.println ("The number was " + number + ".");   
            System.out.println ("You've lost your bet!");       
            bet = bet-bet;// loses bet
         }
      
      }
      
      return bet;
   }


}