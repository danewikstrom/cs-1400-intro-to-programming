import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class GUIMemoryGame
{

   Random r = new Random();
   
   private JFrame main;
   private JOptionPane message;
   private JLabel lblInstructions;
   private JTextField txtInput;
   public int colorNum = 1; 
   private String[] colors = {"red", "white", "yellow", "green", "blue"};
   
   
   public GUIMemoryGame ()//memory game box / constructor
   {
      main = new JFrame ();
      main.setSize (600,300);
      main.setTitle("Memory Game");
      main.setDefaultCloseOperation(main.EXIT_ON_CLOSE);
      main.setLayout(new FlowLayout());
      
      createContents();
      randomColor();
      
      main.setVisible(true);
   }

   public void randomColor ()
   {  
   
      String printout = "How good is your memory? \nTry to memorize this color sequence:\n";
           
      for (int i = 0; i < colors.length; i++)
      {
         colors[i] = colors[r.nextInt(4)];
         printout = printout + colors[i]; 
         
         if (i < (colors.length-1))
         {
            printout = printout + ", ";
         }
         
      }    
        JOptionPane.showMessageDialog(null, printout);   
      
   }
   
   
   private void createContents ()//label and user input 
   {
      lblInstructions = new JLabel();
      txtInput = new JTextField(null, 15);
      
      lblInstructions.setText("Enter color number " + colorNum + ":");
      
      //add action to text box
      txtInput.addActionListener(new GameListener());
      
      main.add(lblInstructions);
      main.add(txtInput);
   }
   
   private class GameListener implements ActionListener
   {
   
      public void actionPerformed (ActionEvent e)
      { 
         String guess = "";
        
         guess = txtInput.getText();
        
         if (colors[colorNum-1].equalsIgnoreCase(guess))//????if user guess equals random color 
         {
            colorNum++;
            
            if(colorNum == 5)//???
            {
               lblInstructions.setText("You win!");
            }
            
            else
            {
            lblInstructions.setText("Enter color number" + colorNum + ":");
            txtInput.setText(""); //clears text box
            }
            
         }
         
         else
         {
            lblInstructions.setText("You lost!");
         }
        
         lblInstructions.setText("Enter color number" + colorNum + ":");
        
      }
   
   }

}