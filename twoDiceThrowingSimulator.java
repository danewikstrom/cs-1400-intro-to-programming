import java.util.Scanner;
import java.util.Random;

public class twoDiceThrowingSimulator
{

   public static void main (String[] args)
   {
      
      Scanner in = new Scanner (System.in);
      Random diceSimulation = new Random();
      
      int numberThrows = 0; //number of rolls the user chooses
      int dieOne = 0;
      int dieTwo = 0;
      int numAsterisk = 0;
      int[] tally = new int [13]; 
      
      
    
      System.out.println ("Welcome to the dice throwing simulator!");
      System.out.println (" ");
      System.out.println ("How many dice rolls would you like to simulate?");
      numberThrows = in.nextInt();
      
      while (numberThrows < 0)
      {
         System.out.println ("Invalid input. The number of simulations must be a positive number.");
         numberThrows = in.nextInt();
      }
      
      for (int i = 2; i < numberThrows; i++)//simulates random number between 1-6 for each die and adds them together
      {
         dieOne = diceSimulation.nextInt(6)+1;
         
         dieTwo = diceSimulation.nextInt(6)+1;
         
         tally[dieOne+dieTwo]++;
      }
      
     
      for (int i = 2; i < tally.length; i++) //calculate percentages 
      {
         System.out.print (i + ": ");
        
         numAsterisk = 100 * tally[i] / numberThrows;
         
      
         for (int j = 2; j <= numAsterisk; j++)//prints percentages as asterisks
         {
            System.out.print ("*");
         } 
      System.out.println();
      }
      
   }

}