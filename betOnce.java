import java.util.*;

public class betOnce
{
   public void highLow (int number, Scanner in)
   {   
      int userChoice = 0;
      
      System.out.println ("Do you want to bet on 1) low or 2) high?");
      userChoice = in.nextInt();
      
      while(userChoice != (1) && userChoice != (2))
      {
         System.out.println ("Invalid input. Only 1 (representing low) or 2 (representing high) are valid inputs. \nDo you want to bet on 1) low or 2) high?");
         userChoice = in.nextInt();
      }
      
      if (number == 0)
      {
         System.out.println ("The number was " + number + ".");          
         System.out.println("You've lost.");
      }
      
      else 
      {
         if (userChoice <=18 && number <=18 || userChoice >=19 && number >=19 )
         {
            System.out.println ("The number was " + number + ".");          
            System.out.println ("You've won!");
         }
         
         if (userChoice <=18 && number >18 || userChoice >18 && number <=18)
         {
            System.out.println ("The number was " + number + ".");          
            System.out.println ("You've lost!");
         }
      
      }
      
   }
}