import java.util.*;

public class distanceCalculator
{

   public static void main (String[] args)
   {
   
      Scanner in = new Scanner (System.in);
   
      double unit = 0;
      double solution = 0;
      String userChoice = "";
       
      
      System.out.println ("To convert miles to kilometers, press M. \nTo convert kilometers to miles, press K.");
      userChoice = in.nextLine();
         
         
      while (!userChoice.equalsIgnoreCase ("M") && (!userChoice.equalsIgnoreCase ("K") && (!userChoice.equalsIgnoreCase ("Q"))))//If not valid input
      {
         System.out.println ("Invalid input. To convert miles to kilometers, press M. \nTo convert kilometers to miles, press K.");
         userChoice = in.nextLine();
      }
      
      
      do
      {
         
         if (userChoice.equalsIgnoreCase ("M"))//miles > kilometers conversion
         {
            System.out.println ("How many miles would you like to convert to kilometers?");
            unit = in.nextDouble();
         
            solution = unit / .62137;
         
            System.out.println (unit + " miles converts to " + solution + " kilometers.");  
            
            in.nextLine();     
         }
        
         if (userChoice.equalsIgnoreCase ("K"))//kilometers > miles conversion
         {
            System.out.println ("How many kilometers would you like to convert to miles?");
            unit = in.nextDouble();
         
            solution = unit * .62137;
         
            System.out.println (unit + " kilometers convert to " + solution + " miles.");
            
            in.nextLine();
         }
         
         System.out.println ("To convert miles to kilometers, press M. \nTo convert kilometers to miles, press K. \nPress Q to quit.");
         userChoice = in.nextLine();
      
      }while (!userChoice.equalsIgnoreCase ("Q"));
   
   }


}