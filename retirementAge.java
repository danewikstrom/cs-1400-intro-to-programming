// importing a java scanner to promt users to answer questions.
import java.util.Scanner;

public class retirementAge
{

   public static void main (String[] args)
   {
   
     // 'Scanner in' is the declaration to declare that I want to build a new scanner.
     // 'new Scanner' is attached to system.in.  
     Scanner in = new Scanner (System.in); 
     
     //"" is initializing to avoid a null.
     String monthOfBirth = "";
    
     //int is the variable needed to ask which day of the month the user was born on.
     int dayOfBirth = 0;
     
     // int is the variable needed to ask which year the user was born.
     int yearOfBirth = 0;
     
     // to determine the year of retirement.
     int yearOfRetirement = 0;
     
     
     //Introducing the objective of the program to the user and asking the first question.
     System.out.println("Due to advances in health care, the age for retirement keeps getting pushed back. \nCurrently the age of retirement is 67 years old. \nTo find out the date when you will retire, answer these few simple questions. \nLets start by entering the month were you born in.");
     monthOfBirth = in.nextLine();
     
     //asking users day of birth.
     System.out.println ("What day of the month were you born on?");
     dayOfBirth = in.nextInt();
     
     //asking user year of birth.
     System.out.println ("What year were you born in?");
     yearOfBirth = in.nextInt();
     
     //To calculate year of retirement.
     yearOfRetirement = yearOfBirth + 67;
     
     // Telling the user the result.
     System.out.println ("Accourding to the information you have provided, you will retire on " + monthOfBirth + " " + dayOfBirth + "," + " " + yearOfRetirement + ".");  
     
   
   }


}