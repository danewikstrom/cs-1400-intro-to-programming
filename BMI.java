import java.util.*;

public class BMI
{

   public static void main (String[] args)
   {
   
      Scanner in = new Scanner (System.in);
      
      double height = 0.0;
      double weight = 0.0;
      double bmi = 0.0;
      boolean hasError = false;
      
      do //asking for height in inches
      {
         try
         {
            hasError = false;
            
            System.out.println ("Enter your height in inches.");
            height = in.nextDouble();
            
            while (height <= 0)
            {
               System.out.println ("The number entered must be a positive number. For example, 5 feet 10 inches = 70.");
               System.out.println (" ");
               System.out.println ("Re-enter your height in inches.");
               height = in.nextDouble();
            }
            
         }
         
         catch (InputMismatchException e)
         {
            //Flush the buffer
            in.nextLine();
            
            System.out.println ("The number entered must be a positive decimal number.");
            System.out.println (" ");
            
            hasError = true;
         }
         
      }while (hasError);
         
         
      do //asking for weight
      {   
         try
         {
            hasError = false;
            
            System.out.println ("Enter your weight in pounds.");
            weight = in.nextDouble();
            
            while (weight <= 0)
            {
               System.out.println ("The number entered must be a positive decimal number.");
               System.out.println (" ");
               System.out.println ("Re-enter your weight in pounds.");
               weight = in.nextDouble();
            }
         
         }
         
         catch (InputMismatchException e)
         {
            //Flush the buffer
            in.nextLine();
            
            System.out.println ("The number entered must be a positive decimal number.");
            System.out.println (" ");
            
            hasError = true;
         
         }
      
      }while (hasError);
      
      bmi = (704*weight) / (height*height);
      
      System.out.println ("Height: " + height);
      System.out.println ("Weight: " + weight);
      System.out.printf ("Your Body Mass Index is %.2f " , bmi);
   
   }


}

